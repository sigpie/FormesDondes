from pythonosc import udp_client
import time
import numpy as np


client = udp_client.SimpleUDPClient("192.168.1.101", 57120)
essaie_msg_osc = np.linspace(-1, 1, 512)
while True:
    client.send_message("/wave", essaie_msg_osc)
    time.sleep(1)