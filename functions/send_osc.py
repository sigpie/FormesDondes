from pythonosc import udp_client


def send_osc(msg, adresse: str = "/wave", ip: str = "127.0.0.1", port: int = 57120):
    client = udp_client.SimpleUDPClient(address=ip, port=port)
    client.send_message(address=adresse, value=msg)


