# rappel pour les coordonnées des pixel [y, x]
import numpy as np
import json
from pythonosc import udp_client
import time
import numpy as np

# sel_cntr est censée être la même chose que ce qui est envoyée par osc
# mais sous forme d'une liste de coordonées (avec les x) et non-normalisée
# pour afficher cette courbe et vérifier qu'on ne la modifie pas
# PS:tout ça parce que le retour de l'oscillo de SuperCollider ne renvoie pas du tout la même chose qu'à l'image

def convert_tabledonde(cntr: list):
    lst_x = []
    lst_y = []
    prev_x = 0
    sel_cntr = []

    """Convertir les valeurs sur l'axe des x des contours en valeurs entre -1 et 1"""
    for coordonne in cntr:

        for y, x in coordonne:
            """ On enregistre une paire de coordonées seulement si la courbe ne revient pas en arrière"""
            if prev_x < x :
                lst_x.append(x)
                lst_y.append(y)
                sel_cntr.append([x, y])
                prev_x = x


    """On faire démarrer les x à zéro"""
    lst_x -= min(lst_x) # inutile non?


    """Normalise les Y entre 1 et -1"""
    #moitier_amplitude_max = ( (max(lst_y) - min(lst_y)) / 2 + min(lst_y))
    # version simplifiée un poil >>>
    moitier_amplitude_max = ( (max(lst_y) + min(lst_y)) / 2 )
    #print(f"max - min :{(max(lst_x) - min(lst_x)) / 2 + min(lst_x)}")
    #print(f"moyenne :{(max(lst_x) + min(lst_x)) / 2 }")
    norm_lst_y = lst_y / moitier_amplitude_max - 1

    return [norm_lst_y, sel_cntr]
    # fonctionel
    # sel_cntr non-fonctionnel

# with open("/home/dr/git/FormesDondes/cntr.json", "r") as f:
#     cntr_brut = np.array(json.load(f))
#     print(type(cntr_brut))
# valeur_normalise = convert_tabledonde(cntr_brut)
# print(valeur_normalise)

# while True:
#     client.send_message("/wave", valeur_normalise)
#
#     print(f"de type : {type(valeur_normalise)}, nbr d'elt : {len(valeur_normalise)}  envoyer a pierre !!!")
#     time.sleep(1)
