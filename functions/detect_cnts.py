import cv2 as cv
import imutils
import numpy as np

def detection_contour(frame, choix:int=1, minimum: int=100, maximum: int=200,):
    gray_frame = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
    # juste en dessous ke ksize peut prendre 1,3,5,7
    if choix == 1:
        return cv.Sobel(gray_frame, cv.CV_64F, 1, 0, ksize=5)
    if choix == 2:
        return cv.Laplacian(gray_frame, cv.CV_64F, ksize=5)
    if choix == 3:
        return cv.Canny(gray_frame, minimum, maximum)
    if choix == 4:
        frame = cv.bilateralFilter(gray_frame, 11, 17, 17)
        edges = cv.Canny(frame, 30, 200)
        cnts = cv.findContours(edges, cv.RETR_TREE, cv.CHAIN_APPROX_NONE)
        cnts = imutils.grab_contours(cnts)
        cnts = sorted(cnts, key=cv.contourArea, reverse=True)[:1]
        black_frame = np.zeros(gray_frame.shape, dtype=np.uint8)
        cv.drawContours(black_frame, cnts, -1, (255, 255, 255), 1)
        return black_frame, cnts
