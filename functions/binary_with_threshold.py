import numpy as np
import cv2 as cv
from lib import seuil_binaire

# ici on peut remplacer l'argument par le chemin d'un fichier
print("Pour quiter: la touche 'q' ")
cap = cv.VideoCapture(0)
while cap.isOpened():
    ret, frame = cap.read()
    # if frame is read correctly ret is True
    if not ret:
        print("pas de reception de frame, "
              "probleme soit avec le chemein "
              "soit avec l'adress de la cam (ret == None)")
        break
    frame_gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
    frame_threshold = seuil_binaire(frame_gray, 3)

    cv.imshow('frame', frame_threshold)
    if cv.waitKey(1) == ord('q') :
        break
cap.release()
cv.destroyAllWindows()