from pythonosc import udp_client
import cv2 as cv
import imutils
import numpy as np

def convert_tabledonde(cntr: list):
    lst_x = []
    lst_y = []
    # Convertir les valeurs sur l'axe des x des contours en valeurs entre -1 et 1,
    for coordonne in cntr:
        # print("elt dans cntr",len(cntr))
        # print(len(coordonne))
        # print(type(coordonne))
        # print("coordonnée[0] : ", coordonne[0])
        # ordonne, abscisse = coordonne[0]
        # print(ordonne, " ", abscisse)
        for y, x in coordonne:
            lst_x.append(x)
            lst_y.append(y)
    lst_x -= min(lst_x)
    moitier_amplitude_max = ((max(lst_y) - min(lst_y)) / 2 + min(lst_y))
    # print(f"max - min :{(max(lst_x) - min(lst_x)) / 2 + min(lst_x)}")
    lst_y = lst_y / moitier_amplitude_max - 1
    # client.send_message("/wave", lst_y)
    return lst_y    # pour le debug
    # fonctionel

# with open("/home/dr/git/FormesDondes/cntr.json", "r") as f:
#     cntr_brut = np.array(json.load(f))
#     print(type(cntr_brut))
# valeur_normalise = convert_tabledonde(cntr_brut)
# print(valeur_normalise)

# while True:
#     client.send_message("/wave", valeur_normalise)
#
#     print(f"de type : {type(valeur_normalise)}, nbr d'elt : {len(valeur_normalise)}  envoyer a pierre !!!")
#     time.sleep(1)


def envoi_osc(msg, ip="127.0.0.1", port=57120, adress_osc="/wave"):
    client = udp_client.SimpleUDPClient(ip, port)
    client.send_message(adress_osc, msg)


def detection_cntr(frame, choix:int=1):
    gray_frame = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
    if choix == 1:
        frame = cv.bilateralFilter(gray_frame, 11, 17, 17)
        edges = cv.Canny(frame, 30, 200)
        cnts = cv.findContours(edges, cv.RETR_TREE, cv.CHAIN_APPROX_NONE)
        cnts = imutils.grab_contours(cnts)
        cnts = sorted(cnts, key=cv.contourArea, reverse=True)[:1]
        black_frame = np.zeros(gray_frame.shape, dtype=np.uint8)
        cv.drawContours(black_frame, cnts, -1, (255, 255, 255), 1)
        return black_frame, cnts

    elif choix == 2:
        """essais avec la technique "sobel and scharr", on ne sais pas se que ça fait"""
        ksize = -1
        gx = cv.Sobel(gray_frame, ddepth=cv.CV_32F, dx=1, dy=0, ksize=ksize)
        # TODO



    else:
        print("Mauvais choix, uniquement entre 1 et 2 !!")
        return("erreur !!")
