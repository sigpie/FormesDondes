def seuil_binaire(frame, choix: int = 1, seuil: int = 127, maximum: int = 255):
    if choix == 1:
        _, img = cv.threshold(frame, seuil, maximum, cv.THRESH_BINARY)
        return img

    elif choix == 2:
        blur = cv.medianBlur(frame, 5)
        return cv.adaptiveThreshold(blur, maximum, cv.ADAPTIVE_THRESH_GAUSSIAN_C, \
                                    cv.THRESH_BINARY, 11, 2)

    elif choix == 3: # separation automatique en deux partie egal de 0 et 255
        blur = cv.GaussianBlur(frame, (5, 5), 0)
        _, img = cv.threshold(blur, 0, maximum, cv.THRESH_BINARY+cv.THRESH_OTSU)
        return img
