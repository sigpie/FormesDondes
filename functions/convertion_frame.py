
def convertion_frame(frame, choix: int = 1):
    """convertion vers, au choix:
        - 1: noir et blanc (niveau de gris) 1 mono
        - 2: YUV  Y': luma correction gama ?, (U, V): Crominence ?
            utiliser pour les format PAL, NTSC
        - 3: HSV    H: Hue (0°, 360°),
                    S: Saturation(1%-100%),
                    V: Value (1%-100%)(teinte saturation valeur)
            approche d'une representation de la perception des couleur par les humains
        - 4: binaire (threshold)
    Return:
        la nouvelle frame
    """
    if choix == 1:
        return cv.cvtColor(frame, cv.COLOR_BGR2GRAY)

    elif choix == 2:
        yuv = cv.cvtColor(frame, cv.COLOR_BGR2YUV)
        y, u, v = cv.split(yuv)
        return yuv, y, u, v

    elif choix == 3:
        hsv = cv.cvtColor(frame, cv.COLOR_BGR2HSV)
        h, s, v = cv.split(hsv)
        return hsv, h, s, v
