import cv2 as cv
import numpy as np
from pythonosc.udp_client import SimpleUDPClient

ip = "127.0.0.1"
port = 1337

client = SimpleUDPClient(ip, port)  # Create client



clicked = False

def on_mouse(event, x, y, flags, param):
    global clicked
    if event == cv.EVENT_LBUTTONDOWN:
        clicked = True

if __name__ == "__main__":
    camera_capture = cv.VideoCapture(0)
    cv.namedWindow("Retour Camera")
    cv.setMouseCallback("Retour Camera", on_mouse)

    o, p = 1, 10
    succes, frame = camera_capture.read()
    while cv.waitKey(1) == -1 and not clicked:
        # yuv, y, u, v = convertion_frame(frame, 2)
        # hsv, h, s, V = convertion_frame(frame, 3)
        # o, p = (v[50][50])/256, (V[100][100])/256
        # # frame = cv.merge((y, h, u))
        # print(type(o), type(p))
        # frame = cv.addWeighted(frame, o, hsv, p, 0)

        frame = detection_contour(frame, minimum=30, maximum=200, choix= 4)
        if len(frame) == 2:
            frame, cnts = frame

        for cnt in cnts:
            print("le nombre de pxl", len(cnt))
            cnt = cnt.ravel()

            i = 0
            lst_x, lst_y = [], []
            for j in cnt:
                if i % 2 == 0:
                    x = cnt[i]
                    y = cnt[i +1]
                    lst_x.append(x)
                    lst_y.append(y)
                i += 1
            lst_x.sort()
            lst_y.sort()
            print(lst_x[0], lst_x[-1])
            print("la difference x : ", lst_x[-1] - lst_x[0])
            print(lst_y[0], lst_y[-1])
            print("la difference y : ", lst_y[-1] - lst_y[0])

            # print(cnt)
            # min_pxl, max_pxl = cnt[0][0].ravel(), cnt[-1][0].ravel()
            # min_x, max_x = min_pxl[1], max_pxl[1]
            # min_y, max_y = min_pxl[0], max_pxl[0]
            # print(f"x min : {min_x}, x max: {max_x}")
            # print(f" y min : {min_y}, y max : {max_y}")
            # print("ecart en derniere et premier : ", min_x - max_x)
        if len(cnt) > 200 and lst_x[-1] - lst_x[0] > 200 and lst_y[-1] - lst_y[0] < 10:

        #     print(f"primier pxl {min_pxl}, dernier pxl {max_pxl}")
            print("dans la boucle pour la sauvegarde")
            cv.imwrite("forme_d_onde.png", frame)

            break
        # frame = detection_contour(frame, 3, minimum=60, maximum=300)
        # client.send_message("/test", )  # Send float message
        # print(f"moyenne detection contour: {np.average(frame)}")
        if frame is not None:
            cv.imshow("Retour Camera", frame)
        success, frame = camera_capture.read()
    camera_capture.release()
    cv.destroyAllWindows()







