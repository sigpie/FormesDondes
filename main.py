import cv2 as cv
import numpy as np
import json
import matplotlib.pyplot as plt

""" a refaire:
            - meilleure detection des contours https://pyimagesearch.com/2021/05/12/image-gradients-with-opencv-sobel-and-scharr/
            - en plus de la normalisation, le premier point et le dernier point qui doivent etre les même.
            - """


from pythonosc import udp_client

from functions.detect_cnts import detection_contour
from functions.convert_table import convert_tabledonde
from functions.send_osc import send_osc
# from functions.draw_wave import draw_wave

# ip = "192.168.1.101"
ip = "127.0.0.1"
port = 57120
address = "/wave"



clicked = False
def on_mouse(event, x, y, flags, param):
    global clicked
    if event == cv.EVENT_LBUTTONDOWN:
        clicked = True


if __name__ == "__main__":
    camera_capture = cv.VideoCapture(0)
    cv.namedWindow("Retour Camera")
    cv.setMouseCallback("Retour Camera", on_mouse)

    normalise_coord = []
    wave = np.array([], np.int32)

    o, p = 1, 10
    succes, frame = camera_capture.read()
    while cv.waitKey(1) == -1 and not clicked:
        # yuv, y, u, v = convertion_frame(frame, 2)
        # hsv, h, s, V = convertion_frame(frame, 3)
        # o, p = (v[50][50])/256, (V[100][100])/256
        # # frame = cv.merge((y, h, u))
        # print(type(o), type(p))
        # frame = cv.addWeighted(frame, o, hsv, p, 0)

        frame = detection_contour(frame, minimum=30, maximum=200, choix= 4)

        if len(frame) == 2:
            frame, cntrs = frame

        for cntr in cntrs:

            if len(cntr) > 1500: # Détermine la longueur minimum pour que le contour soit traité

                # TOUT ça dessous semble pas utilisé, ici, donc je l'enlève
                # en fait c'est passé dans convert_tabledonde
                # lst_x, lst_y = [], []
                # for coordonée in cntr:
                    # for y, x in coordonée:
                        # lst_x.append(x)
                        # lst_y.append(y)

                # lst_x.sort()
                # lst_y.sort()

                """Convertir la courbe en coordonnées normalisées"""
                # La deuxième liste retournée est censée être la même chose que ce qui est envoyée par osc
                # mais sous forme d'une liste de coordonées (avec les x) et non-normalisée
                # pour afficher cette courbe et vérifier qu'on ne la modifie pas
                # PS:tout ça parce que le retour de l'oscillo de SuperCollider ne renvoie pas du tout la même chose qu'à l'image
                normalise_coord, sel_cntr = convert_tabledonde(cntr)

                """ envoie msg osc"""
                send_osc(normalise_coord, address, ip, port)

                """ Enregistre la courbe"""
                wave = cntr
                wave.reshape((-1, 1, 2))

                print("wave : ", wave, " // Plat : ", sel_cntr)

                """ Enregistre la courbe normalisée"""
                normwave = normalise_coord
                # normwave.reshape((-1, 1, 2))
                # print(f"de type : {type(normalise_coord)}, nbr d'elt : {len(normalise_coord)}  envoyée à {ip} !!!")

                # print(normalise_coord)
                # print("et la longueur : ", len(normalise_coord))
                # with open("coordonne_normalise.json", "w") as f:
                #     json.dump(normalise_coord.tolist(), f, indent=2)


        # frame = detection_contour(frame, 3, minimum=60, maximum=300)
        # client.send_message("/test", )  # Send float message
        # print(f"moyenne detection contour: {np.average(frame)}")
        if frame is not None:

            """ Affiche la courbe enregistrée"""
            if len(wave) > 2 :
                # Repasse l'image en RGB pour diffrencier les courbes enregistrées
                frame = cv.cvtColor(frame,cv.COLOR_GRAY2RGB)

                wave = np.array(wave)
                frame = cv.polylines(frame, [wave], True, (255, 255, 0), 1)

                """ On affiche la courbe telle qu'envoyée à OSC """
                # PAS ENCORE FONCTIONNEL
                # normwave = np.array(normwave)
                # frame = cv.polylines(frame, [normwave], True, (155, 80, 255))

            cv.imshow("Retour Camera", frame)
        success, frame = camera_capture.read()
    camera_capture.release()
    cv.destroyAllWindows()
